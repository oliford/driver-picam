package picamJNI;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/** Known parameter definitions, copied from picam.h */
public class PICamParameters {
 
	private static final int PI_V(int v, int c, int n){ return (((c)<<24)+((v)<<16)+(n)); }
	private static final int paramC(int paramID){ return (paramID >> 24) & 0xFF; } 
	private static final int paramV(int paramID){ return (paramID >> 16) & 0xFF; } 
	private static final int paramN(int paramID){ return paramID & 0xFFFF; } 
	
	/*-------------------------------------------------------------------------------------*/
    /* Shutter Timing ---------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int ExposureTime                      = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        23);
    public static final int ShutterTimingMode                 = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   24);
    public static final int ShutterOpeningDelay               = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        46);
    public static final int ShutterClosingDelay               = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        25);
    public static final int ShutterDelayResolution            = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Collection,   47);
    /*-------------------------------------------------------------------------------------*/
    /* Intensifier ------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int EnableIntensifier                 = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   86);
    public static final int IntensifierStatus                 = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         87);
    public static final int IntensifierGain                   = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        88);
    public static final int EMIccdGainControlMode             = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,  123);
    public static final int EMIccdGain                        = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,       124);
    public static final int PhosphorDecayDelay                = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        89);
    public static final int PhosphorDecayDelayResolution      = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Collection,   90);
    public static final int GatingMode                        = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   93);
    public static final int RepetitiveGate                    = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,        94);
    public static final int SequentialStartingGate            = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,        95);
    public static final int SequentialEndingGate              = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,        96);
    public static final int SequentialGateStepCount           = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_Range,        97);
    public static final int SequentialGateStepIterations      = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_Range,        98);
    public static final int DifStartingGate                   = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,       102);
    public static final int DifEndingGate                     = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,       103);
    public static final int BracketGating                     = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  100);
    public static final int IntensifierOptions                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,        101);
    public static final int EnableModulation                  = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  111);
    public static final int ModulationDuration                = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       118);
    public static final int ModulationFrequency               = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       112);
    public static final int RepetitiveModulationPhase         = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       113);
    public static final int SequentialStartingModulationPhase = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       114);
    public static final int SequentialEndingModulationPhase   = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       115);
    public static final int CustomModulationSequence          = PI_V(PICamDefs.valueType_Modulations,   PICamDefs.constraintType_Modulations, 119);
    public static final int PhotocathodeSensitivity           = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,        107);
    public static final int GatingSpeed                       = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,        108);
    public static final int PhosphorType                      = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,        109);
    public static final int IntensifierDiameter               = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,        110);
    /*-------------------------------------------------------------------------------------*/
    /* Analog to Digital Conversion -------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int AdcSpeed                          = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Collection,   33);
    public static final int AdcBitDepth                       = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,   34);
    public static final int AdcAnalogGain                     = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   35);
    public static final int AdcQuality                        = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   36);
    public static final int AdcEMGain                         = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        53);
    public static final int CorrectPixelBias                  = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  106);
    /*-------------------------------------------------------------------------------------*/
    /* Hardware I/O -----------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int TriggerSource                     = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   79);
    public static final int TriggerResponse                   = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   30);
    public static final int TriggerDetermination              = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   31);
    public static final int TriggerFrequency                  = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        80);
    public static final int TriggerTermination                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   81);
    public static final int TriggerCoupling                   = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   82);
    public static final int TriggerThreshold                  = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        83);
    public static final int OutputSignal                      = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   32);
    public static final int InvertOutputSignal                = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   52);
    public static final int AuxOutput                         = PI_V(PICamDefs.valueType_Pulse,         PICamDefs.constraintType_Pulse,        91);
    public static final int EnableSyncMaster                  = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   84);
    public static final int SyncMaster2Delay                  = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        85);
    public static final int EnableModulationOutputSignal      = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  116);
    public static final int ModulationOutputSignalFrequency   = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       117);
    public static final int ModulationOutputSignalAmplitude   = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       120);
    public static final int AnticipateTrigger                 = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  131);
    public static final int DelayFromPreTrigger               = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       132);
    /*-------------------------------------------------------------------------------------*/
    /* Readout Control --------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int ReadoutControlMode                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   26);
    public static final int ReadoutTimeCalculation            = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         27);
    public static final int ReadoutPortCount                  = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,   28);
    public static final int ReadoutOrientation                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         54);
    public static final int KineticsWindowHeight              = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        56);
    public static final int VerticalShiftRate                 = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Collection,   13);
    public static final int Accumulations                     = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_Range,        92);
    public static final int EnableNondestructiveReadout       = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  128);
    public static final int NondestructiveReadoutPeriod       = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       129);
    /*-------------------------------------------------------------------------------------*/
    /* Data Acquisition -------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int Rois                              = PI_V(PICamDefs.valueType_Rois,          PICamDefs.constraintType_Rois,         37);
    public static final int NormalizeOrientation              = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   39);
    public static final int DisableDataFormatting             = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   55);
    public static final int ReadoutCount                      = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_Range,        40);
    public static final int ExactReadoutCountMaximum          = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_None,         77);
    public static final int PhotonDetectionMode               = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,  125);
    public static final int PhotonDetectionThreshold          = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,       126);
    public static final int PixelFormat                       = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   41);
    public static final int FrameSize                         = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         42);
    public static final int FrameStride                       = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         43);
    public static final int FramesPerReadout                  = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         44);
    public static final int ReadoutStride                     = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         45);
    public static final int PixelBitDepth                     = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         48);
    public static final int ReadoutRateCalculation            = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         50);
    public static final int OnlineReadoutRateCalculation      = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         99);
    public static final int FrameRateCalculation              = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         51);
    public static final int Orientation                       = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         38);
    public static final int TimeStamps                        = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,   68);
    public static final int TimeStampResolution               = PI_V(PICamDefs.valueType_LargeInteger,  PICamDefs.constraintType_Collection,   69);
    public static final int TimeStampBitDepth                 = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,   70);
    public static final int TrackFrames                       = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   71);
    public static final int FrameTrackingBitDepth             = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,   72);
    public static final int GateTracking                      = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,  104);
    public static final int GateTrackingBitDepth              = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,  105);
    public static final int ModulationTracking                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_Collection,  121);
    public static final int ModulationTrackingBitDepth        = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Collection,  122);
    /*-------------------------------------------------------------------------------------*/
    /* Sensor Information -----------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int SensorType                        = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         57);
    public static final int CcdCharacteristics                = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         58);
    public static final int SensorActiveWidth                 = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         59);
    public static final int SensorActiveHeight                = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         60);
    public static final int SensorActiveLeftMargin            = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         61);
    public static final int SensorActiveTopMargin             = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         62);
    public static final int SensorActiveRightMargin           = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         63);
    public static final int SensorActiveBottomMargin          = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         64);
    public static final int SensorMaskedHeight                = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         65);
    public static final int SensorMaskedTopMargin             = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         66);
    public static final int SensorMaskedBottomMargin          = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         67);
    public static final int SensorSecondaryMaskedHeight       = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         49);
    public static final int SensorSecondaryActiveHeight       = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_None,         74);
    public static final int PixelWidth                        = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,          9);
    public static final int PixelHeight                       = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         10);
    public static final int PixelGapWidth                     = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         11);
    public static final int PixelGapHeight                    = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         12);
    /*-------------------------------------------------------------------------------------*/
    /* Sensor Layout ----------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int ActiveWidth                       = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         1);
    public static final int ActiveHeight                      = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         2);
    public static final int ActiveLeftMargin                  = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         3);
    public static final int ActiveTopMargin                   = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         4);
    public static final int ActiveRightMargin                 = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         5);
    public static final int ActiveBottomMargin                = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         6);
    public static final int MaskedHeight                      = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         7);
    public static final int MaskedTopMargin                   = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,         8);
    public static final int MaskedBottomMargin                = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        73);
    public static final int SecondaryMaskedHeight             = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        75);
    public static final int SecondaryActiveHeight             = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        76);
    /*-------------------------------------------------------------------------------------*/
    /* Sensor Cleaning --------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int CleanSectionFinalHeight           = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        17);
    public static final int CleanSectionFinalHeightCount      = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        18);
    public static final int CleanSerialRegister               = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   19);
    public static final int CleanCycleCount                   = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        20);
    public static final int CleanCycleHeight                  = PI_V(PICamDefs.valueType_Integer,       PICamDefs.constraintType_Range,        21);
    public static final int CleanBeforeExposure               = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   78);
    public static final int CleanUntilTrigger                 = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   22);
    public static final int StopCleaningOnPreTrigger          = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  130);
    /*-------------------------------------------------------------------------------------*/
    /* Sensor Temperature -----------------------------------------------------------------*/
    /*-------------------------------------------------------------------------------------*/
    public static final int SensorTemperatureSetPoint         = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_Range,        14);
    public static final int SensorTemperatureReading          = PI_V(PICamDefs.valueType_FloatingPoint, PICamDefs.constraintType_None,         15);
    public static final int SensorTemperatureStatus           = PI_V(PICamDefs.valueType_Enumeration,   PICamDefs.constraintType_None,         16);
    public static final int DisableCoolingFan                 = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,   29);
    public static final int EnableSensorWindowHeater          = PI_V(PICamDefs.valueType_Boolean,       PICamDefs.constraintType_Collection,  127);
    /*-------------------------------------------------------------------------------------*/
    

    /* Some reflection based collection of parameters */
	private static final HashMap<Integer, String> knownParams = getKnownParams();
	
	private static final HashMap<Integer, String> getKnownParams(){
		HashMap<Integer, String> knownParams = new HashMap<Integer, String>();
				
		Field[] fields = PICamParameters.class.getDeclaredFields();
		for (Field f : fields) {
		    if (Modifier.isStatic(f.getModifiers()) && Modifier.isFinal(f.getModifiers()) && f.getType() == int.class ){		    	
		    	try {
		    		knownParams.put(f.getInt(null), f.getName());
				} catch (Exception e) {
					e.printStackTrace();
				}
		    } 
		}
		
		return knownParams;
	}
	
	public static String getName(int paramID){
		return knownParams.get(paramID);
	}
	
	public static final int getType(int paramID){ return paramV(paramID); }
	
	public static final int getConstraintType(int paramID){ return paramC(paramID); }
}
