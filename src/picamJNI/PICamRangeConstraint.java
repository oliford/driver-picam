package picamJNI;

public class PICamRangeConstraint {
	public int scope;
	public int severity;
	public boolean empty_set;
    public double minimum;
    public double maximum;
    public double increment;
    public double excluded_values[];    
    public double outlying_values[];
    
    @Override
    public String toString() {
    	String ret = ((scope == PICamDefs.constraintScope_Dependent) ? "Dependent " : "Independent, ")
    		+ ((severity == PICamDefs.constraintSeverity_Error) ? "Error " : "Warning, ")
    		+ (empty_set ? "Empty, " : "Not empty, ")
    		+ ", min=" + minimum + ", max=" + maximum + ", inc=" + increment;
    		
    	if(excluded_values != null && excluded_values.length > 0){
    		ret += ", Excluded=[";
    		for(int i=0; i < excluded_values.length; i++)
    			ret += excluded_values[i] + ", ";
    		ret += "]";
    	}
    	
    	if(outlying_values != null && outlying_values.length > 0){
    		ret += ", Outlying=[";
    		for(int i=0; i < outlying_values.length; i++)
    			ret += outlying_values[i] + ", ";
    		ret += "]";
    	}
    	
    	return ret;
    }
}
