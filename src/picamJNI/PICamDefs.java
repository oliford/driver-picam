package picamJNI;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;

/** Static definitions for PICam */
public abstract class PICamDefs {
	public static final int MAX_ENUM_STRINGS = 100;
	
	public static final int valueType_Integer       = 1;
    public static final int valueType_Boolean       = 3;
    public static final int valueType_Enumeration   = 4;
    public static final int valueType_LargeInteger  = 6;
    public static final int valueType_FloatingPoint = 2;
    public static final int valueType_Rois          = 5;
    public static final int valueType_Pulse         = 7;
    public static final int valueType_Modulations   = 8;
    
    /** None = read-only */
    public static final int constraintType_None        = 1;
	public static final int constraintType_Range       = 2;
    public static final int constraintType_Collection  = 3;
    public static final int constraintType_Rois        = 4;
    public static final int constraintType_Pulse       = 5;
    public static final int constraintType_Modulations = 6;
	
   
    public static final int constraintScope_Independent = 1;
    public static final int constraintScope_Dependent   = 2;
    
    public static final int constraintSeverity_Error   = 1;
    public static final int constraintSeverity_Warning = 2;
    
    public static final int constraintCategory_Capable     = 1;
    public static final int constraintCategory_Required    = 2;
    public static final int constraintCategory_Recommended = 3;
    
    public static final int timeStampsMask_ExposureStarted = 1;
    public static final int timeStampsMask_ExposureEnded = 2;
	
}
