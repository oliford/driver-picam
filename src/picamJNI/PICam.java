package picamJNI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import otherSupport.SettingsManager;


public class PICam {
	static {
		System.load(SettingsManager.defaultGlobal().getPathProperty("picamJNI.jniLibPath", "/usr/local/lib") + "/picamJNI.so");
	}
	
	private static final native void _InitializeLibrary();	
	private static final native void _UninitializeLibrary();
	private static boolean libraryInited = false;

	private static final native byte[] _ConnectDemoCamera(int modelID, String serialNumber);
	private static final native long[] _OpenFirstCamera();
	private static final native long[] _OpenCamera(byte cameraID[]);
	
	public static PicamCameraID ConnectDemoCamera(int modelID, String serialNumber) {
		InitializeLibrary();
		return new PicamCameraID(_ConnectDemoCamera(modelID, serialNumber));
	}
	
	public final static void InitializeLibrary(){
		if(!libraryInited){
			_InitializeLibrary();
			libraryInited = true;
		}
	}
	
	public static PICam OpenCamera(PicamCameraID cameraID){ 
		InitializeLibrary();
		
		return new PICam(_OpenCamera(cameraID.getStructData())); 
	}

	private static final native long[] _Advanced_OpenCameraDevice(byte cameraID[]);
	public static PICam Advanced_OpenCameraDevice(PicamCameraID cameraID){ 
		InitializeLibrary();
		
		return new PICam(_Advanced_OpenCameraDevice(cameraID.getStructData())); 
	}
		
	public static PICam OpenFirstCamera(){
		InitializeLibrary();
		
		return new PICam(_OpenFirstCamera()); 
	}		
	
	/** Handle to C++ object in JNI land */
	private long hModel, hDevice;
	
	/** private. A class is returned by one of OpenCamera or OpenFirstCamera */
	private PICam(long handles[]){
		this.hModel = handles[0];
		this.hDevice = handles[1];
	}
			
	private native byte[] _getCameraID();
	public PicamCameraID getCameraID() {
		return new PicamCameraID(_getCameraID());
	}
		
	public native void close();
	
	/** @param ret[2]{ returnErrorsMask, returnReadoutCount } */
	public ByteBuffer acquire(long readout_count, int readout_time_out, int ret[]){
		ByteBuffer buff = _acquire(readout_count, readout_time_out, ret);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		return buff;
	}
	private native ByteBuffer _acquire(long readout_count, int readout_time_out, int ret[]);
	
	
	public native void startAcquisition();		
	public native void stopAcquisition();
	
	
	public native int getParameterIntegerDefaultValue(int parameter);
	public native long getParameterLargeIntegerDefaultValue(int parameter);
	public native float getParameterFloatingPointDefaultValue(int parameter);
	//public native xxx getParameterRoisDefaultValue(int parameter);
	//public native xxx getParameterPulseDefaultValue(int parameter);
	//public native xxx getParameterModulationsDefaultValue(int parameter);
		
	
	//public native boolean canSetParameterOnline(int parameter);
	//public native boolean canReadParameter(int parameter);
		
	public native int readParameterIntegerValue(int parameter);
	public native float readParameterFloatingPointValue(int parameter);
		
	public native int getParameterIntegerValue(int parameter);
	public native void setParameterIntegerValue(int parameter, int value);
	public native boolean canSetParameterIntegerValue(int parameter, int value);
	
	public native long getParameterLargeIntegerValue(int parameter);
	public native void setParameterLargeIntegerValue(int parameter, long value);
	public native boolean canSetParameterLargeIntegerValue(int parameter, long value);
	
	public native float getParameterFloatingPointValue(int parameter);
	public native void setParameterFloatingPointValue(int parameter, float value);
	public native boolean canSetParameterFloatingPointValue(int parameter, float value);
	
	private native byte[] _getParameterRoisValue(int parameter);
	public PICamROIs getParameterRoisValue(int parameter){ return new PICamROIs(_getParameterRoisValue(parameter)); }
	
	private native void _setParameterRoisValue(int parameter, byte structData[]);
	public void setParameterRoisValue(int parameter, PICamROIs rois){ 
		_setParameterRoisValue(parameter, rois.toStructData()); 
	}
	
	
	private native void _getParameterCollectionConstraint(int parameter, int category, PICamCollectionConstraint constraintObj);
	
	public PICamCollectionConstraint getParameterCollectionConstraint(int parameter, int category){
		PICamCollectionConstraint ret = new PICamCollectionConstraint();
		_getParameterCollectionConstraint(parameter, category, ret);
		return ret;
	}
		
	private native void _getParameterRangeConstraint(int parameter, int category, PICamRangeConstraint constraintObj);
	
	public PICamRangeConstraint getParameterRangeConstraint(int parameter, int category){
		PICamRangeConstraint ret = new PICamRangeConstraint();
		_getParameterRangeConstraint(parameter, category, ret);
		return ret;
	}
	
	/** Returns list of supported parameters for this camera (PICamDefs.parameter_...)  */
	public native int[] getParameters(); 
	
	/** Returns the String for a given value of a given enumeration type */  
	public native String GetEnumerationString(int type, int value);

	
	/** Returns the enumeration type of a given parameter */
	public native int GetParameterEnumeratedType(int parameter);

	public native void SetAcquisitionBuffer(ByteBuffer buffer);

	
	private native int _waitForAcquisitionUpdate(int readout_time_out, byte structData[]);
	
	public static class AcquisitionStatus {
		public boolean running;
		public int errorsMask;
		public double readout_rate;
		
		public AcquisitionStatus(){ }
			
		private AcquisitionStatus(byte structData[]) { setStructData(structData); }
			
		private void setStructData(byte structData[]){
			ByteBuffer buff = ByteBuffer.wrap(structData);
			buff.order(ByteOrder.LITTLE_ENDIAN);
			running = (buff.getInt(0) != 0);
			errorsMask = buff.getInt(4);
			readout_rate = buff.getDouble(8);
		}
	}

	public int waitForAcquisitionUpdate(int readout_time_out){
		return _waitForAcquisitionUpdate(readout_time_out, null);
	}

	public int waitForAcquisitionUpdate(int readout_time_out, AcquisitionStatus status){
		byte structData[] = new byte[16];
		int nImages = _waitForAcquisitionUpdate(readout_time_out, structData);
		status.setStructData(structData);
		return nImages;
	}
	
	public native boolean isAcquisitionRunning();
	
	/** @returns list of failed parameter IDs */
	public native int[] CommitParameters();
	
	
	public native void registerForAcquisitionUpdated(PICamAcquisitionUpdatedCallback callbackDef);
	
	public native void unregisterForAcquisitionUpdated();
	
}
