package picamJNI;

public interface PICamAcquisitionUpdatedCallback {
	public void acquisitionUpdatedCallback(int bufferPos, int readout_count, boolean running, int errorsMask, int readout_rate);
}
