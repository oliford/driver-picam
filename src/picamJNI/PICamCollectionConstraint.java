package picamJNI;


public class PICamCollectionConstraint {
	public int scope;
	public int severity;
	public double values[];

	@Override
    public String toString() {
    	String ret = ((scope == PICamDefs.constraintScope_Dependent) ? "Dependent " : "Independent, ")
    		+ ((severity == PICamDefs.constraintSeverity_Error) ? "Error " : "Warning, ")
    		+ ", values=";
    	
    	if(values == null){
    		ret += "(null)";
    	}else{
    		ret += "[";
    		for(int i=0; i < values.length; i++)
    			ret += values[i] + ", ";
    		ret += "]";
    	}
    	
    	return ret;
    }
}
