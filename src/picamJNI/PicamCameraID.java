package picamJNI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map.Entry;

public class PicamCameraID {
	
	private static HashMap<Integer, String> models = new HashMap<Integer, String>();
	
	static{
		models.put(1400, "PIMteSeries");
	    /* PI-MTE 1024 Series ----------------------------------------------------*/
	    models.put(1401, "PIMte1024Series");
	    models.put(1402, "PIMte1024F");
	    models.put(1403, "PIMte1024B");
	    models.put(1405, "PIMte1024BR");
	    models.put(1404, "PIMte1024BUV");
	    /* PI-MTE 1024FT Series --------------------------------------------------*/
	    models.put(1406, "PIMte1024FTSeries");
	    models.put(1407, "PIMte1024FT");
	    models.put(1408, "PIMte1024BFT");
	    /* PI-MTE 1300 Series ----------------------------------------------------*/
	    models.put(1412, "PIMte1300Series");
	    models.put(1413, "PIMte1300B");
	    models.put(1414, "PIMte1300R");
	    models.put(1415, "PIMte1300BR");
	    /* PI-MTE 2048 Series ----------------------------------------------------*/
	    models.put(1416, "PIMte2048Series");
	    models.put(1417, "PIMte2048B");
	    models.put(1418, "PIMte2048BR");
	    /* PI-MTE 2K Series ------------------------------------------------------*/
	    models.put(1409, "PIMte2KSeries");
	    models.put(1410, "PIMte2KB");
	    models.put(1411, "PIMte2KBUV");
	    /*------------------------------------------------------------------------*/
	    /* PIXIS Series (76) -----------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(0, "PixisSeries");
	    /* PIXIS 100 Series ------------------------------------------------------*/
	    models.put(1, "Pixis100Series");
	    models.put(2, "Pixis100F");
	    models.put(6, "Pixis100B");
	    models.put(3, "Pixis100R");
	    models.put(4, "Pixis100C");
	    models.put(5, "Pixis100BR");
	    models.put(54, "Pixis100BExcelon");
	    models.put(55, "Pixis100BRExcelon");
	    models.put(7, "PixisXO100B");
	    models.put(8, "PixisXO100BR");
	    models.put(68, "PixisXB100B");
	    models.put(69, "PixisXB100BR");
	    /* PIXIS 256 Series ------------------------------------------------------*/
	    models.put(26, "Pixis256Series");
	    models.put(27, "Pixis256F");
	    models.put(29, "Pixis256B");
	    models.put(28, "Pixis256E");
	    models.put(30, "Pixis256BR");
	    models.put(31, "PixisXB256BR");
	    /* PIXIS 400 Series ------------------------------------------------------*/
	    models.put(37, "Pixis400Series");
	    models.put(38, "Pixis400F");
	    models.put(40, "Pixis400B");
	    models.put(39, "Pixis400R");
	    models.put(41, "Pixis400BR");
	    models.put(56, "Pixis400BExcelon");
	    models.put(57, "Pixis400BRExcelon");
	    models.put(42, "PixisXO400B");
	    models.put(70, "PixisXB400BR");
	    /* PIXIS 512 Series ------------------------------------------------------*/
	    models.put(43, "Pixis512Series");
	    models.put(44, "Pixis512F");
	    models.put(45, "Pixis512B");
	    models.put(46, "Pixis512BUV");
	    models.put(58, "Pixis512BExcelon");
	    models.put(49, "PixisXO512F");
	    models.put(50, "PixisXO512B");
	    models.put(48, "PixisXF512F");
	    models.put(47, "PixisXF512B");
	    /* PIXIS 1024 Series -----------------------------------------------------*/
	    models.put(9, "Pixis1024Series");
	    models.put(10, "Pixis1024F");
	    models.put(11, "Pixis1024B");
	    models.put(13, "Pixis1024BR");
	    models.put(12, "Pixis1024BUV");
	    models.put(59, "Pixis1024BExcelon");
	    models.put(60, "Pixis1024BRExcelon");
	    models.put(16, "PixisXO1024F");
	    models.put(14, "PixisXO1024B");
	    models.put(15, "PixisXO1024BR");
	    models.put(17, "PixisXF1024F");
	    models.put(18, "PixisXF1024B");
	    models.put(71, "PixisXB1024BR");
	    /* PIXIS 1300 Series -----------------------------------------------------*/
	    models.put(51, "Pixis1300Series");
	    models.put(52, "Pixis1300F");
	    models.put(75, "Pixis1300F_2");
	    models.put(53, "Pixis1300B");
	    models.put(73, "Pixis1300BR");
	    models.put(61, "Pixis1300BExcelon");
	    models.put(62, "Pixis1300BRExcelon");
	    models.put(65, "PixisXO1300B");
	    models.put(66, "PixisXF1300B");
	    models.put(72, "PixisXB1300R");
	    /* PIXIS 2048 Series -----------------------------------------------------*/
	    models.put(20, "Pixis2048Series");
	    models.put(21, "Pixis2048F");
	    models.put(22, "Pixis2048B");
	    models.put(67, "Pixis2048BR");
	    models.put(63, "Pixis2048BExcelon");
	    models.put(74, "Pixis2048BRExcelon");
	    models.put(23, "PixisXO2048B");
	    models.put(25, "PixisXF2048F");
	    models.put(24, "PixisXF2048B");
	    /* PIXIS 2K Series -------------------------------------------------------*/
	    models.put(32, "Pixis2KSeries");
	    models.put(33, "Pixis2KF");
	    models.put(34, "Pixis2KB");
	    models.put(36, "Pixis2KBUV");
	    models.put(64, "Pixis2KBExcelon");
	    models.put(35, "PixisXO2KB");
	    /*------------------------------------------------------------------------*/
	    /* Quad-RO Series (104) --------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(100, "QuadroSeries");
	    models.put(101, "Quadro4096");
	    models.put(103, "Quadro4096_2");
	    models.put(102, "Quadro4320");
	    /*------------------------------------------------------------------------*/
	    /* ProEM Series (214) ----------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(200, "ProEMSeries");
	    /* ProEM 512 Series ------------------------------------------------------*/
	    models.put(203, "ProEM512Series");
	    models.put(201, "ProEM512B");
	    models.put(205, "ProEM512BK");
	    models.put(204, "ProEM512BExcelon");
	    models.put(206, "ProEM512BKExcelon");
	    /* ProEM 1024 Series -----------------------------------------------------*/
	    models.put(207, "ProEM1024Series");
	    models.put(202, "ProEM1024B");
	    models.put(208, "ProEM1024BExcelon");
	    /* ProEM 1600 Series -----------------------------------------------------*/
	    models.put(209, "ProEM1600Series");
	    models.put(212, "ProEM1600xx2B");
	    models.put(210, "ProEM1600xx2BExcelon");
	    models.put(213, "ProEM1600xx4B");
	    models.put(211, "ProEM1600xx4BExcelon");
	    /*------------------------------------------------------------------------*/
	    /* ProEM+ Series (614) ---------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(600, "ProEMPlusSeries");
	    /* ProEM+ 512 Series -----------------------------------------------------*/
	    models.put(603, "ProEMPlus512Series");
	    models.put(601, "ProEMPlus512B");
	    models.put(605, "ProEMPlus512BK");
	    models.put(604, "ProEMPlus512BExcelon");
	    models.put(606, "ProEMPlus512BKExcelon");
	    /* ProEM+ 1024 Series ----------------------------------------------------*/
	    models.put(607, "ProEMPlus1024Series");
	    models.put(602, "ProEMPlus1024B");
	    models.put(608, "ProEMPlus1024BExcelon");
	    /* ProEM+ 1600 Series ----------------------------------------------------*/
	    models.put(609, "ProEMPlus1600Series");
	    models.put(612, "ProEMPlus1600xx2B");
	    models.put(610, "ProEMPlus1600xx2BExcelon");
	    models.put(613, "ProEMPlus1600xx4B");
	    models.put(611, "ProEMPlus1600xx4BExcelon");
	    /*------------------------------------------------------------------------*/
	    /* ProEM-HS Series (1209) ------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(1200, "ProEMHSSeries");
	    /* ProEM-HS 512 Series ---------------------------------------------------*/
	    models.put(1201, "ProEMHS512Series");
	    models.put(1202, "ProEMHS512B");
	    models.put(1207, "ProEMHS512BK");
	    models.put(1203, "ProEMHS512BExcelon");
	    models.put(1208, "ProEMHS512BKExcelon");
	    /* ProEM-HS 1024 Series --------------------------------------------------*/
	    models.put(1204, "ProEMHS1024Series");
	    models.put(1205, "ProEMHS1024B");
	    models.put(1206, "ProEMHS1024BExcelon");
	    /*------------------------------------------------------------------------*/
	    /* PI-MAX3 Series (303) --------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(300, "PIMax3Series");
	    models.put(301, "PIMax31024I");
	    models.put(302, "PIMax31024x256");
	    /*------------------------------------------------------------------------*/
	    /* PI-MAX4 Series (721) --------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(700, "PIMax4Series");
	    /* PI-MAX4 1024i Series --------------------------------------------------*/
	    models.put(703, "PIMax41024ISeries");
	    models.put(701, "PIMax41024I");
	    models.put(704, "PIMax41024IRF");
	    /* PI-MAX4 1024f Series --------------------------------------------------*/
	    models.put(710, "PIMax41024FSeries");
	    models.put(711, "PIMax41024F");
	    models.put(712, "PIMax41024FRF");
	    /* PI-MAX4 1024x256 Series -----------------------------------------------*/
	    models.put(705, "PIMax41024x256Series");
	    models.put(702, "PIMax41024x256");
	    models.put(706, "PIMax41024x256RF");
	    /* PI-MAX4 2048 Series ---------------------------------------------------*/
	    models.put(716, "PIMax42048Series");
	    models.put(717, "PIMax42048F");
	    models.put(718, "PIMax42048B");
	    models.put(719, "PIMax42048FRF");
	    models.put(720, "PIMax42048BRF");
	    /* PI-MAX4 512EM Series --------------------------------------------------*/
	    models.put(708, "PIMax4512EMSeries");
	    models.put(707, "PIMax4512EM");
	    models.put(709, "PIMax4512BEM");
	    /* PI-MAX4 1024EM Series -------------------------------------------------*/
	    models.put(713, "PIMax41024EMSeries");
	    models.put(715, "PIMax41024EM");
	    models.put(714, "PIMax41024BEM");
	    /*------------------------------------------------------------------------*/
	    /* PyLoN Series (439) ----------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(400, "PylonSeries");
	    /* PyLoN 100 Series ------------------------------------------------------*/
	    models.put(418, "Pylon100Series");
	    models.put(404, "Pylon100F");
	    models.put(401, "Pylon100B");
	    models.put(407, "Pylon100BR");
	    models.put(425, "Pylon100BExcelon");
	    models.put(426, "Pylon100BRExcelon");
	    /* PyLoN 256 Series ------------------------------------------------------*/
	    models.put(419, "Pylon256Series");
	    models.put(409, "Pylon256F");
	    models.put(410, "Pylon256B");
	    models.put(411, "Pylon256E");
	    models.put(412, "Pylon256BR");
	    /* PyLoN 400 Series ------------------------------------------------------*/
	    models.put(420, "Pylon400Series");
	    models.put(405, "Pylon400F");
	    models.put(402, "Pylon400B");
	    models.put(408, "Pylon400BR");
	    models.put(427, "Pylon400BExcelon");
	    models.put(428, "Pylon400BRExcelon");
	    /* PyLoN 1024 Series -----------------------------------------------------*/
	    models.put(421, "Pylon1024Series");
	    models.put(417, "Pylon1024B");
	    models.put(429, "Pylon1024BExcelon");
	    /* PyLoN 1300 Series -----------------------------------------------------*/
	    models.put(422, "Pylon1300Series");
	    models.put(406, "Pylon1300F");
	    models.put(403, "Pylon1300B");
	    models.put(438, "Pylon1300R");
	    models.put(432, "Pylon1300BR");
	    models.put(430, "Pylon1300BExcelon");
	    models.put(433, "Pylon1300BRExcelon");
	    /* PyLoN 2048 Series -----------------------------------------------------*/
	    models.put(423, "Pylon2048Series");
	    models.put(415, "Pylon2048F");
	    models.put(434, "Pylon2048B");
	    models.put(416, "Pylon2048BR");
	    models.put(435, "Pylon2048BExcelon");
	    models.put(436, "Pylon2048BRExcelon");
	    /* PyLoN 2K Series -------------------------------------------------------*/
	    models.put(424, "Pylon2KSeries");
	    models.put(413, "Pylon2KF");
	    models.put(414, "Pylon2KB");
	    models.put(437, "Pylon2KBUV");
	    models.put(431, "Pylon2KBExcelon");
	    /*------------------------------------------------------------------------*/
	    /* PyLoN-IR Series (904) -------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(900, "PylonirSeries");
	    /* PyLoN-IR 1024 Series --------------------------------------------------*/
	    models.put(901, "Pylonir1024Series");
	    models.put(902, "Pylonir102422");
	    models.put(903, "Pylonir102417");
	    /*------------------------------------------------------------------------*/
	    /* PIoNIR Series (502) ---------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(500, "PionirSeries");
	    models.put(501, "Pionir640");
	    /*------------------------------------------------------------------------*/
	    /* NIRvana Series (802) --------------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(800, "NirvanaSeries");
	    models.put(801, "Nirvana640");
	    /*------------------------------------------------------------------------*/
	    /* NIRvana ST Series (1302) ----------------------------------------------*/
	    /*------------------------------------------------------------------------*/
	    models.put(1300, "NirvanaSTSeries");
	    models.put(1301, "NirvanaST640");
	    models.put(1100, "NirvanaLNSeries");
	    models.put(1101, "NirvanaLN640");
	    /*------------------------------------------------------------------------*/
	}
	
	public static int getModelID(String modelName) {
		for(Entry<Integer, String> entry : models.entrySet()){
			if(modelName.equals(entry.getValue()))
				return entry.getKey();				
		}
		throw new IllegalArgumentException("Unidentified camera model '"+modelName+"'");
	}

	public static final int computerInterface_Usb2            = 1;
    public static final int computerInterface_1394A           = 2;
    public static final int computerInterface_GigabitEthernet = 3;
    
    public static final int stringSize_SensorName     =  64;
    public static final int stringSize_SerialNumber   =  64;
    
    public PicamCameraID(byte structData[]) {
		this.structData = structData;
	}
    
    public static int findModel(String modelName) {
    	
    	for(Entry<Integer,String> entry : models.entrySet())
    		if(modelName.equals(entry.getValue()))
    			return entry.getKey();
    	
    	throw new RuntimeException("Model " + modelName + " no known");
    }
    
    public PicamCameraID(String modelName, int computerInterface, String serial, String sensor) {
    	this(findModel(modelName), computerInterface, serial, sensor);
    }
    
    public PicamCameraID(int modelNum, int computerInterface, String serial, String sensor) {
    	structData = new byte[4 + 4 + stringSize_SensorName + stringSize_SerialNumber];
		ByteBuffer buf = ByteBuffer.wrap(structData);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putInt(modelNum);
		buf.putInt(computerInterface);
				
		buf.put(sensor.getBytes(), 0, Math.min(stringSize_SensorName, sensor.length()));
		buf.position(8 + stringSize_SensorName);
		buf.put(serial.getBytes(), 0, Math.min(stringSize_SerialNumber, serial.length()));
		
	}

	private byte structData[];
    
    public byte[] getStructData() { return structData; }
    
    public int getModelID(){
    	return ByteBuffer.wrap(structData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer().get(0);
    }
        
    public String getModelName(){
    	return models.get(getModelID());
    }
    
    public int getComputerInterface(){
    	return ByteBuffer.wrap(structData).asIntBuffer().get(1);
    }    
        
    public String getSensorName(){
    	return new String(structData, 8, stringSize_SensorName).trim();    	
    }
    
	public String getSerialNumber(){
    	return new String(structData, 8 + stringSize_SensorName, stringSize_SerialNumber).trim();
	}
}
