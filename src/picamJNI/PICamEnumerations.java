package picamJNI;

/** Known Enumeration definitions, copied from picam.h */
public class PICamEnumerations {
	

	public static final int ccdCharacteristicsMask_None                 = 0x000;
	public static final int ccdCharacteristicsMask_BackIlluminated      = 0x001;
	public static final int ccdCharacteristicsMask_DeepDepleted         = 0x002;
	public static final int ccdCharacteristicsMask_OpenElectrode        = 0x004;
	public static final int ccdCharacteristicsMask_UVEnhanced           = 0x008;
	public static final int ccdCharacteristicsMask_ExcelonEnabled       = 0x010;
	public static final int ccdCharacteristicsMask_SecondaryMask        = 0x020;
	public static final int ccdCharacteristicsMask_Multiport            = 0x040;
	public static final int ccdCharacteristicsMask_AdvancedInvertedMode = 0x080;
	public static final int ccdCharacteristicsMask_HighResistivity      = 0x100;

	public static final int EMIccdGainControlMode_Optimal = 1;
	public static final int EMIccdGainControlMode_Manual  = 2;

	public static final int gateTrackingMask_None  = 0x0;
	public static final int gateTrackingMask_Delay = 0x1;
	public static final int gateTrackingMask_Width = 0x2;

	public static final int gatingMode_Repetitive = 1;
	public static final int gatingMode_Sequential = 2;
	public static final int gatingMode_Custom     = 3;

	public static final int gatingSpeed_Fast = 1;
	public static final int gatingSpeed_Slow = 2;

	public static final int intensifierOptionsMask_None                = 0x0;
	public static final int intensifierOptionsMask_McpGating           = 0x1;
	public static final int intensifierOptionsMask_SubNanosecondGating = 0x2;
	public static final int intensifierOptionsMask_Modulation          = 0x4;

	public static final int intensifierStatus_PoweredOff = 1;
	public static final int intensifierStatus_PoweredOn  = 2;

	public static final int modulationTrackingMask_None                  = 0x0;
	public static final int modulationTrackingMask_Duration              = 0x1;
	public static final int modulationTrackingMask_Frequency             = 0x2;
	public static final int modulationTrackingMask_Phase                 = 0x4;
	public static final int modulationTrackingMask_OutputSignalFrequency = 0x8;

	public static final int orientationMask_Normal              = 0x0;
	public static final int orientationMask_FlippedHorizontally = 0x1;
	public static final int orientationMask_FlippedVertically   = 0x2;

	public static final int outputSignal_NotReadingOut       =  1;
	public static final int outputSignal_ShutterOpen         =  2;
	public static final int outputSignal_Busy                =  3;
	public static final int outputSignal_AlwaysLow           =  4;
	public static final int outputSignal_AlwaysHigh          =  5;
	public static final int outputSignal_Acquiring           =  6; 
	public static final int outputSignal_ShiftingUnderMask   =  7;
	public static final int outputSignal_Exposing            =  8;
	public static final int outputSignal_EffectivelyExposing =  9;
	public static final int outputSignal_ReadingOut          = 10;
	public static final int outputSignal_WaitingForTrigger   = 11;
	
	public static final int phosphorType_P43 = 1;
	public static final int phosphorType_P46 = 2;
	
	public static final int photocathodeSensitivity_RedBlue          =  1;
	public static final int photocathodeSensitivity_SuperRed         =  7;
	public static final int photocathodeSensitivity_SuperBlue        =  2;
	public static final int photocathodeSensitivity_UV               =  3;
	public static final int photocathodeSensitivity_SolarBlind       = 10;
	public static final int photocathodeSensitivity_Unigen2Filmless  =  4;
	public static final int photocathodeSensitivity_InGaAsFilmless   =  9;
	public static final int photocathodeSensitivity_HighQEFilmless   =  5;
	public static final int photocathodeSensitivity_HighRedFilmless  =  8;
	public static final int photocathodeSensitivity_HighBlueFilmless =  6;

	public static final int photonDetectionMode_Disabled     = 1;
	public static final int photonDetectionMode_Thresholding = 2;
	public static final int photonDetectionMode_Clipping     = 3;
	
	public static final int pixelFormat_Monochrome16Bit = 1;
	
	public static final int readoutControlMode_FullFrame       = 1;  
	public static final int readoutControlMode_FrameTransfer   = 2;
	public static final int readoutControlMode_Interline       = 5;
	public static final int readoutControlMode_Kinetics        = 3;
	public static final int readoutControlMode_SpectraKinetics = 4;
	public static final int readoutControlMode_Dif             = 6;
	
	public static final int sensorTemperatureStatus_Unlocked = 1;
	public static final int sensorTemperatureStatus_Locked   = 2;
	
	public static final int sensorType_Ccd    = 1;
	public static final int sensorType_InGaAs = 2;
	
	public static final int shutterTimingMode_Normal            = 1;
	public static final int shutterTimingMode_AlwaysClosed      = 2;
	public static final int shutterTimingMode_AlwaysOpen        = 3;
	public static final int shutterTimingMode_OpenBeforeTrigger = 4;
	
	public static final int timeStampsMask_None            = 0x0;
	public static final int timeStampsMask_ExposureStarted = 0x1;
	public static final int timeStampsMask_ExposureEnded   = 0x2;
	
	public static final int triggerCoupling_AC = 1;
	public static final int triggerCoupling_DC = 2;
	public static final int triggerDetermination_PositivePolarity = 1;
	public static final int triggerDetermination_NegativePolarity = 2;
	public static final int triggerDetermination_RisingEdge       = 3;
	public static final int triggerDetermination_FallingEdge      = 4;
	public static final int triggerResponse_NoResponse               = 1;
	public static final int triggerResponse_ReadoutPerTrigger        = 2;
	public static final int triggerResponse_ShiftPerTrigger          = 3;
	public static final int triggerResponse_ExposeDuringTriggerPulse = 4;
	public static final int triggerResponse_StartOnSingleTrigger     = 5;
	public static final int triggerSource_External = 1;
	public static final int triggerSource_Internal = 2;
	public static final int triggerTermination_FiftyOhms     = 1;
	public static final int triggerTermination_HighImpedance = 2;

	public static final int valueAccess_ReadOnly         = 1;
	public static final int valueAccess_ReadWriteTrivial = 3;
	public static final int valueAccess_ReadWrite        = 2;

	public static final int constraintScope_Independent = 1;
	public static final int constraintScope_Dependent   = 2;

	public static final int constraintSeverity_Error   = 1;
	public static final int constraintSeverity_Warning = 2;

	public static final int constraintCategory_Capable     = 1;
	public static final int constraintCategory_Required    = 2;
	public static final int constraintCategory_Recommended = 3;

	public static final int roisConstraintRulesMask_None                  = 0x00;
	public static final int roisConstraintRulesMask_XBinningAlignment     = 0x01;
	public static final int roisConstraintRulesMask_YBinningAlignment     = 0x02;
	public static final int roisConstraintRulesMask_HorizontalSymmetry    = 0x04;
	public static final int roisConstraintRulesMask_VerticalSymmetry      = 0x08;
	public static final int roisConstraintRulesMask_SymmetryBoundsBinning = 0x10;

	public static final int acquisitionErrorsMask_None           = 0x0;
	public static final int acquisitionErrorsMask_DataLost       = 0x1;
	public static final int acquisitionErrorsMask_ConnectionLost = 0x2;
	
}
