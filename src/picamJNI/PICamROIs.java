package picamJNI;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import picamJNI.PICamROIs.PICamROI;


public class PICamROIs {
    
	public static class PICamROI {
		//non-PI things useful in ImageProc
		public String name;
		public boolean enabled;
		
    	public int x;
    	public int width;
    	public int x_binning;
    	public int y;
    	public int height;
    	public int y_binning;
    	
    	@Override
    	public String toString() {
    		return "{"+name+","
    				+ (enabled?"Enabled,":"Disabled,")
    				+ x + "," + width + "," + x_binning + ","
    				+ y + "," + height + "," + y_binning + "}";
    	}
    }
    
    private ArrayList<PICamROI> rois;
    
    public PICamROIs(){
    	rois = new ArrayList<PICamROI>();
    }
    
    public PICamROIs(byte structData[]){
    	int nROIs = structData.length / (6*4);
    	
    	ByteBuffer buff = ByteBuffer.wrap(structData);
    	buff.order(ByteOrder.LITTLE_ENDIAN);
    	
    	rois = new ArrayList<PICamROI>(nROIs);
    	
    	for(int i=0; i < nROIs; i++){
    		PICamROI roi = new PICamROI();
    		roi.name = "Loaded_"+i;
    		roi.enabled = true;
    		roi.x = buff.getInt();
    		roi.width = buff.getInt();
    		roi.x_binning = buff.getInt();
    		roi.y = buff.getInt();
    		roi.height = buff.getInt();
    		roi.y_binning = buff.getInt();
    		rois.add(roi);
    	}
    }
    
    public void updateFromStructData(byte structData[]){
    	int nROIs = structData.length / (6*4);
    	
    	ByteBuffer buff = ByteBuffer.wrap(structData);
    	buff.order(ByteOrder.LITTLE_ENDIAN);
    	
    	rois = new ArrayList<PICamROI>(nROIs);
    	
    	for(int i=0; i < nROIs; i++){
    		PICamROI roi = new PICamROI();
    		roi.name = "Auto_"+i;
    		roi.enabled = true;
    		roi.x = buff.getInt();
    		roi.width = buff.getInt();
    		roi.x_binning = buff.getInt();
    		roi.y = buff.getInt();
    		roi.height = buff.getInt();
    		roi.y_binning = buff.getInt();
    		rois.add(roi);
    	}
    }
	
    public byte[] toStructData(){
    	synchronized (rois) {
    		int nEnabled = 0;
    		for(PICamROI roi : rois)
    			if(roi.enabled)
    				nEnabled++;
    		
			byte structData[] = new byte[nEnabled * (6*4)];
	
	    	ByteBuffer buff = ByteBuffer.wrap(structData);
	    	buff.order(ByteOrder.LITTLE_ENDIAN);
	
	    	for(PICamROI roi : rois){	    
	    		if(!roi.enabled)
	    			continue;
	    		buff.putInt(roi.x);
	    		buff.putInt(roi.width);
	    		buff.putInt(roi.x_binning);
	    		buff.putInt(roi.y);
	    		buff.putInt(roi.height);
	    		buff.putInt(roi.y_binning);
	    	}
	    	
	    	return structData;
    	}
    }

    public void addROI(int x, int width, int x_binning, int y, int height, int y_binning){
    	PICamROI roi = new PICamROI();
    	roi.x = x;
    	roi.width = width;
    	roi.x_binning = x_binning;
    	roi.y = y;
    	roi.height = height;
    	roi.y_binning = y_binning;
    	rois.add(roi);
    }

	public void addROI(PICamROI roi) {
		synchronized (rois) {
			rois.add(roi);
    	}
	} 
	
	public PICamROI getROI(String name){
		for(PICamROI roi : rois)
			if(name.equals(roi.name))
				return roi;
		return null;
	}

	@Override
	public String toString() {
		StringBuffer strB = new StringBuffer();
		strB.append("ROIs[");
		synchronized (rois) {
			for(PICamROI roi : rois)
				strB.append(roi);
		}
		strB.append("]");
		return strB.toString();
	}

	public void remove(PICamROI roi) {
		rois.remove(roi);
	}
	
	public ArrayList<PICamROI> getROIListCopy() {
		synchronized (rois) {
			return (ArrayList<PICamROI>)rois.clone();
		}
	}

	public void addArraysToMap(HashMap<String, Object> map, String prefix, boolean enabledOnly) {
		synchronized (rois) {
				
			ArrayList<String> roisNames = new ArrayList<String>();
			ArrayList<Integer> roisX = new ArrayList<Integer>();
			ArrayList<Integer> roisWidth = new ArrayList<Integer>();
			ArrayList<Integer> roisXBinning = new ArrayList<Integer>();
			ArrayList<Integer> roisY = new ArrayList<Integer>();
			ArrayList<Integer> roisHeight = new ArrayList<Integer>();
			ArrayList<Integer> roisYBinning = new ArrayList<Integer>();
			ArrayList<Integer> roisEnabled = new ArrayList<Integer>();
			
			int idx = 0;
			for(PICamROI roi : rois){
				if(enabledOnly && !roi.enabled)
					continue;
				
				roisNames.add(roi.name);
				roisX.add(roi.x);
				roisWidth.add(roi.width);
				roisXBinning.add(roi.x_binning);
				roisY.add(roi.y);
				roisHeight.add(roi.height);
				roisYBinning.add(roi.y_binning);
				roisEnabled.add(roi.enabled ? 1 : 0);
				
				idx++;
			}
			
			map.put(prefix + "/names", roisNames.toArray(new String[idx]));
			map.put(prefix + "/x", roisX.toArray(new Integer[idx]));
			map.put(prefix + "/width", roisWidth.toArray(new Integer[idx]));
			map.put(prefix + "/x_binning", roisXBinning.toArray(new Integer[idx]));
			map.put(prefix + "/y", roisY.toArray(new Integer[idx]));
			map.put(prefix + "/height", roisHeight.toArray(new Integer[idx]));
			map.put(prefix + "/y_binning", roisYBinning.toArray(new Integer[idx]));			
			map.put(prefix + "/enabled", roisEnabled.toArray(new Integer[idx]));
		}
	}

	public void sortROIsList() {
		Collections.sort(rois, new Comparator<PICamROI>() {
			/*@Override
			public int compare(PICamROI r1, PICamROI r2) {
				return Integer.compare(r1.y, r2.y);
			}*/
			public int compare(PICamROI r1, PICamROI r2) {
				return r1.name.compareTo(r2.name);
			}
			
		});
	}

	public void clear() {
		synchronized (rois) {
			rois.clear();
		}		
	}
	
	/** Jog given ROI by given amount, keep edges at edges and adjust width/height and binning accordingly */
	public void jog(PICamROI roi, int sensorWidth, int sensorHeight, int dx, int dy) {
		boolean clampLeft = (roi.x == 0);
		boolean clampRight = ((roi.x + roi.width) == sensorWidth);

		roi.x += dx;

		if(clampRight || (roi.x + roi.width > sensorWidth)) {
			int dw = roi.x + roi.width - sensorWidth;
			if(roi.x_binning == roi.width) //catch the full binning case.
				roi.x_binning -= dw;
			roi.width -= dw; 
		}

		if(clampLeft || roi.x < 0) {
			int w = roi.x + roi.width;
			roi.x = 0;				
			if(roi.x_binning == roi.width) //catch the full binning case.
				roi.x_binning = w;
			roi.width = w; 
		}	

		boolean clampTop = (roi.y == 0);
		boolean clampBottom = ((roi.y + roi.height) == sensorHeight);

		roi.y += dy;

		if(clampBottom || (roi.y + roi.height > sensorHeight)) {
			int dh = roi.y + roi.height - sensorHeight;
			if(roi.y_binning == roi.height) //catch the full binning case.
				roi.y_binning -= dh;
			roi.height -= dh; 
		}

		if(clampTop || roi.y < 0) {
			int h = roi.y + roi.height;
			roi.y = 0;				
			if(roi.y_binning == roi.height) //catch the full binning case.
				roi.y_binning = h;
			roi.height = h; 
		}	

	}
}
