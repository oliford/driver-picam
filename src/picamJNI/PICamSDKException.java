package picamJNI;

import java.util.HashMap;

public class PICamSDKException extends RuntimeException {

	public static final int ENUM_NOT_DEFINED = 17;
	public static final int TIMEOUT = 32;
	
	private String functionName;
	private int errorCode;
	private static final HashMap<Integer, String> errorStrings = new HashMap<Integer, String>();
	
	{
		errorStrings.put(0, "None");
		errorStrings.put(4, "Unexpected Error");
		errorStrings.put(3, "Unexpected Null Pointer");
		errorStrings.put(35, "Invalid Pointer");
		errorStrings.put(39, "Invalid Count");
		errorStrings.put(42, "Invalid Operation");
		errorStrings.put(43, "Operation Canceled");
		errorStrings.put(1, "Library Not Initialized");
		errorStrings.put(5, "Library Already Initialized");
		errorStrings.put(17, "Enumeration Value Not Defined");
		errorStrings.put(18, "Not Discovering Cameras");
		errorStrings.put(19, "Already Discovering Cameras");
		errorStrings.put(34, "No Cameras Available");
		errorStrings.put(7, "Camera Already Opened");
		errorStrings.put(8, "Invalid Camera ID");
		errorStrings.put(9, "Invalid Handle");
		errorStrings.put(15, "Device Communication Failed");
		errorStrings.put(23, "Device Disconnected");
		errorStrings.put(24, "Device Open Elsewhere");
		errorStrings.put(6, "Invalid Demo Model");
		errorStrings.put(21, "Invalid Demo Serial Number");
		errorStrings.put(22, "Demo Already Connected");
		errorStrings.put(40, "Demo Not Supported");
		errorStrings.put(11, "Parameter Has Invalid Value Type");
		errorStrings.put(13, "Parameter Has Invalid Constraint Type");
		errorStrings.put(12, "Parameter Does Not Exist");
		errorStrings.put(10, "Parameter Value Is Read Only");
		errorStrings.put(2, "Invalid Parameter Value");
		errorStrings.put(38, "Invalid Constraint Category");
		errorStrings.put(14, "Parameter Value Is Irrelevant");
		errorStrings.put(25, "Parameter Is Not Onlineable");
		errorStrings.put(26, "Parameter Is Not Readable");
		errorStrings.put(28, "Invalid Parameter Values");
		errorStrings.put(29, "Parameters Not Committed");
		errorStrings.put(30, "Invalid Acquisition Buffer");
		errorStrings.put(36, "Invalid Readout Count");
		errorStrings.put(37, "Invalid Readout Time Out");
		errorStrings.put(31, "Insufficient Memory");
		errorStrings.put(20, "Acquisition In Progress");
		errorStrings.put(27, "Acquisition Not In Progress");
		errorStrings.put(32, "Time Out Occurred");
		errorStrings.put(33, "Acquisition Updated Handler Registered");
		errorStrings.put(44, "Invalid Acquisition State");
		errorStrings.put(41, "Nondestructive Readout Enabled");

		errorStrings.put(-73, "JNI alloction failed");
	}

	public static final String getErrorString(int returnCode){
			//convert to string here if possible
			String str = errorStrings.get(returnCode);
						
			return (str != null) ? str : ("Unknown PICam SDK error '" + String.format("%x", returnCode) + "'.");
	}
	
	public PICamSDKException(int errorCode) {
		this(getErrorString(errorCode));	
		
		this.functionName = "?";
		this.errorCode = errorCode;		
	}
	
	public PICamSDKException(String functionName, int errorCode) {
		this(functionName + " returned the error "+String.format("%x", errorCode)+": " + getErrorString(errorCode));	
		
		this.functionName = functionName;
		this.errorCode = errorCode;		
	}
	
	public PICamSDKException(String errorString) { 
		super(errorString);
		this.errorCode = -1;
	}

	public int getErrorCode() { return errorCode; }

	public String getErrorString(){ return getErrorString(errorCode); }			

}
