

#include "picamJNI_PICam.h"
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include <picam.h>
#include <picam_advanced.h>


PicamHandle getModelHandle(JNIEnv *env, jobject obj){
	jclass javaClass = env->GetObjectClass(obj);
	jfieldID f = env->GetFieldID(javaClass, "hModel", "J");
	long l = env->GetLongField(obj, f);
	return (PicamHandle)l;
}

PicamHandle getDeviceHandle(JNIEnv *env, jobject obj){
	jclass javaClass = env->GetObjectClass(obj);
	jfieldID f = env->GetFieldID(javaClass, "hDevice", "J");
	long l = env->GetLongField(obj, f);
	return (PicamHandle)l;
}


/* Check return code from AndorSDK functions and throw exception if necessary */
void checkReturn(JNIEnv *env, PicamError returnCode){
	if(returnCode == PicamError_None)
		return; //all OK

	// Find and instantiate the exception
	jclass exClass = env->FindClass("picamJNI/PICamSDKException");

	jmethodID constructor = env->GetMethodID(exClass, "<init>", "(I)V");

	jobject exception = env->NewObject(exClass, constructor, (jint)returnCode);

	// Throw the exception. Since this is native code,
	// execution continues, and the execution will be abruptly
	// interrupted at the point in time when we return to the VM. 
	// The calling code will perform the early return back to Java code.
	env->Throw((jthrowable) exception);

	// Clean up local reference
	env->DeleteLocalRef(exClass);
}


/*
 * Class:     picamJNI_PICam
 * Method:    _InitializeLibrary
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam__1InitializeLibrary
  (JNIEnv *env, jclass cls){
	PicamError ret = Picam_InitializeLibrary();
	checkReturn(env,ret);
	
}

/*
 * Class:     picamJNI_PICam
 * Method:    _UninitializeLibrary
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam__1UninitializeLibrary
  (JNIEnv *env, jclass cls){
	PicamError ret = Picam_UninitializeLibrary();
	checkReturn(env,ret);
}

/*
 * Class:     picamJNI_PICam
 * Method:    _OpenFirstCamera
 * Signature: ()[J
 */
JNIEXPORT jlongArray JNICALL Java_picamJNI_PICam__1OpenFirstCamera
  (JNIEnv *env, jclass cls){
	PicamHandle camera;
	PicamError ret = Picam_OpenFirstCamera(&camera);
	checkReturn(env,ret);

	PicamHandle device, model;
	PicamAdvanced_GetCameraDevice(camera, &device);
	PicamAdvanced_GetCameraModel(camera, &model);
		
	jlongArray retArr = env->NewLongArray(2);
	env->SetLongArrayRegion(retArr, 0, 1, (int64_t*)&model);
	env->SetLongArrayRegion(retArr, 1, 1, (int64_t*)&device);
	return retArr;

}

/*
 * Class:     picamJNI_PICam
 * Method:    _ConnectDemoCamera
 * Signature: (ILjava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL Java_picamJNI_PICam__1ConnectDemoCamera
  (JNIEnv *env, jclass cls, jint modelID, jstring jStrSerialNumber){

	//byte array to return ID structure
	jbyteArray retArr = env->NewByteArray(sizeof(PicamCameraID));
	if (retArr == NULL){
		fprintf(stderr, "PICam: ConnectDemoCamera(): Couldn't allocate byte array in JNI land.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}

	jbyte *buffer = env->GetByteArrayElements(retArr, 0);

	const char *cstrSerialNumber = env->GetStringUTFChars(jStrSerialNumber, NULL);

	PicamError ret = Picam_ConnectDemoCamera((PicamModel)modelID, cstrSerialNumber, (PicamCameraID*)buffer);
	checkReturn(env,ret);

	
	env->ReleaseByteArrayElements(retArr, buffer, 0);	
	env->ReleaseStringUTFChars(jStrSerialNumber, cstrSerialNumber);

	return retArr;
}	
	

/*
 * Class:     picamJNI_PICam
 * Method:    _getCameraID
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_picamJNI_PICam__1getCameraID
  (JNIEnv *env, jobject obj){

	PicamHandle camera = getModelHandle(env, obj);

	//byte array to return ID structure
	jbyteArray retArr = env->NewByteArray(sizeof(PicamCameraID));
	if (retArr == NULL){
		fprintf(stderr, "PICam: getCameraID(): Couldn't allocate byte array in JNI land.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}
	jbyte *buffer = env->GetByteArrayElements(retArr, 0);

	PicamError ret = Picam_GetCameraID(camera, (PicamCameraID*)buffer);

	env->ReleaseByteArrayElements(retArr, buffer, 0);

	checkReturn(env,ret);	
	

	return retArr;
}

/*
 * Class:     picamJNI_PICam
 * Method:    _OpenCamera
 * Signature: ([B)[J
 */
JNIEXPORT jlongArray JNICALL Java_picamJNI_PICam__1OpenCamera
  (JNIEnv *env, jclass cls, jbyteArray cameraIDbytes){

	PicamHandle camera;

	jbyte *buffer = env->GetByteArrayElements(cameraIDbytes, 0);

	PicamError ret = Picam_OpenCamera((PicamCameraID*)buffer, &camera);


	env->ReleaseByteArrayElements(cameraIDbytes, buffer, 0);

	checkReturn(env,ret);

	PicamHandle device, model;
	PicamAdvanced_GetCameraDevice(camera, &device);
	PicamAdvanced_GetCameraModel(camera, &model);
		
	jlongArray retArr = env->NewLongArray(2);
	env->SetLongArrayRegion(retArr, 0, 1, (int64_t*)&model);
	env->SetLongArrayRegion(retArr, 1, 1, (int64_t*)&device);
	return retArr;
}


/*
 * Class:     picamJNI_PICam
 * Method:    _Advanced_OpenCameraDevice
 * Signature: ([B)[J
 */
JNIEXPORT jlongArray JNICALL Java_picamJNI_PICam__1Advanced_1OpenCameraDevice
   (JNIEnv *env, jclass cls, jbyteArray cameraIDbytes){

	PicamHandle camera;

	jbyte *buffer = env->GetByteArrayElements(cameraIDbytes, 0);

	PicamError ret = PicamAdvanced_OpenCameraDevice((PicamCameraID*)buffer, &camera);

	env->ReleaseByteArrayElements(cameraIDbytes, buffer, 0);

	checkReturn(env,ret);

	PicamHandle device, model;
	PicamAdvanced_GetCameraDevice(camera, &device);
	PicamAdvanced_GetCameraModel(camera, &model);

	jlongArray retArr = env->NewLongArray(2);
	env->SetLongArrayRegion(retArr, 0, 1, (int64_t*)&model);
	env->SetLongArrayRegion(retArr, 1, 1, (int64_t*)&device);
	return retArr;
}

/*
 * Class:     picamJNI_PICam
 * Method:    close
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_close
  (JNIEnv *env, jobject obj){
	PicamHandle camera = getModelHandle(env, obj);	
	PicamError ret = Picam_CloseCamera(camera);
	checkReturn(env,ret);	
}


/*
 * Class:     picamJNI_PICam
 * Method:    getParameterIntegerValue
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_picamJNI_PICam_getParameterIntegerValue
  (JNIEnv *env, jobject obj, jint parameter){
	piint value = 0;

	PicamHandle camera = getModelHandle(env, obj);	
	PicamError ret = Picam_GetParameterIntegerValue(camera, (PicamParameter)parameter, &value);
	checkReturn(env, ret);	

	return value;
}



/*
 * Class:     picamJNI_PICam
 * Method:    getParameterFloatingPointValue
 * Signature: (I)F
 */
JNIEXPORT jfloat JNICALL Java_picamJNI_PICam_getParameterFloatingPointValue
  (JNIEnv *env, jobject obj, jint parameter){
	piflt value = 0;

	PicamHandle camera = getModelHandle(env, obj);	
	PicamError ret = Picam_GetParameterFloatingPointValue(camera, (PicamParameter)parameter, &value);
	checkReturn(env, ret);	

	return value;
}


/*
 * Class:     picamJNI_PICam
 * Method:    setParameterLargeIntegerValue
 * Signature: (IJ)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_setParameterLargeIntegerValue
  (JNIEnv *env, jobject obj, jint parameter, jlong value){
	PicamHandle camera = getModelHandle(env, obj);
	
	PicamError ret = Picam_SetParameterLargeIntegerValue(camera, (PicamParameter)parameter, value);
	checkReturn(env, ret);	
}


/*
 * Class:     picamJNI_PICam
 * Method:    setParameterIntegerValue
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_setParameterIntegerValue
  (JNIEnv *env, jobject obj, jint parameter, jint value){
	PicamHandle camera = getModelHandle(env, obj);
	
	PicamError ret = Picam_SetParameterIntegerValue(camera, (PicamParameter)parameter, value);
	checkReturn(env, ret);	
}

/*
 * Class:     picamJNI_PICam
 * Method:    setParameterFloatingPointValue
 * Signature: (IF)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_setParameterFloatingPointValue
  (JNIEnv *env, jobject obj, jint parameter, jfloat value){
	PicamHandle camera = getModelHandle(env, obj);
	
	PicamError ret = Picam_SetParameterFloatingPointValue(camera, (PicamParameter)parameter, value);
	checkReturn(env, ret);	
}
/*
 * Class:     picamJNI_PICam
 * Method:    acquire
 * Signature: (JI[I)Ljava/nio/ByteBuffer;
 */
JNIEXPORT jobject JNICALL Java_picamJNI_PICam__1acquire
  (JNIEnv *env, jobject obj, jlong readout_count, jint readout_time_out, jintArray retArr){

	PicamHandle camera = getModelHandle(env, obj);	
	
	PicamAvailableData data;
	PicamAcquisitionErrorsMask errors;

	PicamError ret = Picam_Acquire(camera, readout_count, readout_time_out, &data, &errors);

	env->SetIntArrayRegion(retArr, 0, 1, (int32_t*)&errors);
	env->SetIntArrayRegion(retArr, 1, 1, (int32_t*)&data.readout_count);

	checkReturn(env,ret);	

	//need to know readout stride, to know how big buffer is
	piint readoutstride = 0;
	Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutStride, &readoutstride );

	//create ByteBuffer from data
	jobject byteBuff = env->NewDirectByteBuffer(data.initial_readout, readoutstride * data.readout_count );
	if(byteBuff == NULL){
		fprintf(stderr, "PICam: acquire(): Couldn't create direct byte buffer from acquired data in JNI land.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}

	return byteBuff;
}


/*
 * Class:     picamJNI_PICam
 * Method:    getParameters
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_picamJNI_PICam_getParameters
  (JNIEnv *env, jobject obj){

	PicamHandle camera = getModelHandle(env, obj);	
	const PicamParameter *parameter_array = NULL;
	piint parameter_count = 0;

	PicamError ret = Picam_GetParameters(camera, &parameter_array, &parameter_count);

	checkReturn(env,ret);	

	if(parameter_array == NULL){
		fprintf(stderr, "PICam: getParameters(): Picam_GetParameters didn't allocate any memory.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return NULL;
	}

	jintArray retArr = env->NewIntArray(parameter_count);
	if (retArr == NULL){
		fprintf(stderr, "PICam: getParameters(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		Picam_DestroyParameters(parameter_array);
		return NULL;
	}

	jint *retArrPtr = env->GetIntArrayElements(retArr, 0);
	
	memcpy(retArrPtr, parameter_array, parameter_count*sizeof(jint));

	env->ReleaseIntArrayElements(retArr, retArrPtr, 0);
	Picam_DestroyParameters(parameter_array);

	return retArr;
}

	
/*
 * Class:     picamJNI_PICam
 * Method:    GetEnumerationString
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_picamJNI_PICam_GetEnumerationString
  (JNIEnv *env, jobject obj, jint type, jint value){

	const pichar *cStr = NULL;

	PicamError ret = Picam_GetEnumerationString((PicamEnumeratedType)type, value, &cStr);
	
	jstring jStr = NULL;
	if(cStr != NULL){
		jStr = env->NewStringUTF(cStr); 
		Picam_DestroyString(cStr);
	}

	checkReturn(env,ret);	

	return jStr;
}



/*
 * Class:     picamJNI_PICam
 * Method:    GetParameterEnumeratedType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_picamJNI_PICam_GetParameterEnumeratedType
  (JNIEnv *env, jobject obj, jint parameter){

	PicamHandle camera = getModelHandle(env, obj);
	PicamEnumeratedType enumType = (PicamEnumeratedType)0; //invalidate
	
	PicamError ret = Picam_GetParameterEnumeratedType(camera, (PicamParameter)parameter, &enumType);

	checkReturn(env,ret);

	return enumType;	
}


/*
 * Class:     picamJNI_PICam
 * Method:    getParameterLargeIntegerValue
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_picamJNI_PICam_getParameterLargeIntegerValue
  (JNIEnv *env, jobject obj, jint parameter){
	pi64s value = 0;

	PicamHandle camera = getModelHandle(env, obj);	
	PicamError ret = Picam_GetParameterLargeIntegerValue(camera, (PicamParameter)parameter, &value);
	checkReturn(env, ret);	

	return value;
}

/*
 * Class:     picamJNI_PICam
 * Method:    SetAcquisitionBuffer
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_SetAcquisitionBuffer
  (JNIEnv *env, jobject obj, jobject jByteBuffer){

	PicamHandle device = getDeviceHandle(env, obj);	

	jbyte *cBufferPtr = (jbyte *)env->GetDirectBufferAddress(jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "PICam: SetAcquisitionBuffer(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return;
	}
	long bufferLen = env->GetDirectBufferCapacity(jByteBuffer);

	PicamAcquisitionBuffer piBuffer;
	piBuffer.memory = cBufferPtr;
    	piBuffer.memory_size = bufferLen;
	
	PicamError ret = PicamAdvanced_SetAcquisitionBuffer(device, &piBuffer);
	printf("PicamAdvanced_SetAcquisitionBuffer() buffer starts at %p\n", piBuffer.memory);
	checkReturn(env, ret);	

}


/*
 * Class:     picamJNI_PICam
 * Method:    _waitForAcquisitionUpdate
 * Signature: (I[B)I
 */
JNIEXPORT jint JNICALL Java_picamJNI_PICam__1waitForAcquisitionUpdate
  (JNIEnv *env, jobject obj, jint readout_time_out, jbyteArray jarrStructData){

	PicamHandle camera = getModelHandle(env, obj);	

	PicamAvailableData available;
	//PicamAcquisitionStatus status;
	available.readout_count = 0;

	jbyte *buffer = env->GetByteArrayElements(jarrStructData, 0);

	PicamError ret = Picam_WaitForAcquisitionUpdate(camera, readout_time_out, &available,(PicamAcquisitionStatus*)buffer);

	env->ReleaseByteArrayElements(jarrStructData, buffer, 0);

	checkReturn(env, ret);

	printf("Picam_WaitForAcquisitionUpdate read %li starting at %p\n", available.readout_count, available.initial_readout);	
	fflush(stdout);

	return available.readout_count;
}


/*
 * Class:     picamJNI_PICam
 * Method:    isAcquisitionRunning
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_picamJNI_PICam_isAcquisitionRunning
  (JNIEnv *env, jobject obj){
	PicamHandle camera = getModelHandle(env, obj);	
	pibln running = false;

	PicamError ret =  Picam_IsAcquisitionRunning(camera, &running);

	checkReturn(env, ret);

	return running;
}

	

/*
 * Class:     picamJNI_PICam
 * Method:    startAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_startAcquisition
  (JNIEnv *env, jobject obj){
	PicamHandle camera = getModelHandle(env, obj);		
	PicamError ret =  Picam_StartAcquisition(camera);
	checkReturn(env, ret);
}

/*
 * Class:     picamJNI_PICam
 * Method:    stopAcquisition
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_stopAcquisition
  (JNIEnv *env, jobject obj){
	PicamHandle camera = getModelHandle(env, obj);		
	PicamError ret =  Picam_StopAcquisition(camera);
	checkReturn(env, ret);
}


/*
 * Class:     picamJNI_PICam
 * Method:    CommitParameters
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL Java_picamJNI_PICam_CommitParameters
  (JNIEnv *env, jobject obj){
	PicamHandle camera = getModelHandle(env, obj);		

	const PicamParameter* failed_parameter_array = NULL;
	piint failed_parameter_count = 0;

	PicamError ret =  Picam_CommitParameters(camera, &failed_parameter_array, &failed_parameter_count);
	checkReturn(env, ret);

	jintArray retArr = env->NewIntArray(failed_parameter_count);
	if (retArr == NULL){
		fprintf(stderr, "PICam: CommitParameters(): Couldn't allocate int array in JNI land.");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		if(failed_parameter_array != NULL)
			Picam_DestroyParameters(failed_parameter_array);
		return NULL;
	}

	if(failed_parameter_count > 0){
		if(failed_parameter_array == NULL){
			fprintf(stderr, "PICam: CommitParameters(): Picam_CommitParameters didn't allocate any memory and count is non-zero.");
			fflush(stderr);
			checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
			return NULL;
		}


		jint *retArrPtr = env->GetIntArrayElements(retArr, 0);
	
		memcpy(retArrPtr, failed_parameter_array, failed_parameter_count*sizeof(jint));

		env->ReleaseIntArrayElements(retArr, retArrPtr, 0);
	}
		
	if(failed_parameter_array != NULL)
		Picam_DestroyParameters(failed_parameter_array);

	return retArr;
	
}

/*
 * Class:     picamJNI_PICam
 * Method:    _getParameterCollectionConstraint
 * Signature: (IILpicamJNI/PICamCollectionConstraint;)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam__1getParameterCollectionConstraint
  (JNIEnv *env, jobject obj, jint parameter, jint category, jobject constraintObj){
	PicamHandle camera = getModelHandle(env, obj);

	const PicamCollectionConstraint *constraint = NULL;

	PicamError ret = Picam_GetParameterCollectionConstraint(camera, (PicamParameter)parameter, (PicamConstraintCategory)category, &constraint);

	if(constraint != NULL){
		jclass constraintClass = env->GetObjectClass(constraintObj);
		jfieldID fieldID;

		fieldID = env->GetFieldID(constraintClass, "scope", "I");
		env->SetIntField(constraintObj, fieldID, constraint->scope);

		fieldID = env->GetFieldID(constraintClass, "severity", "I");
		env->SetIntField(constraintObj, fieldID, constraint->severity);

		jdoubleArray valuesArr = env->NewDoubleArray(constraint->values_count);
		if (valuesArr != NULL){
			env->SetDoubleArrayRegion(valuesArr, 0, constraint->values_count, (double*)constraint->values_array);
		}
		fieldID = env->GetFieldID(constraintClass, "values", "[D");
		env->SetObjectField(constraintObj, fieldID, valuesArr);


		Picam_DestroyCollectionConstraints(constraint);
	}

	checkReturn(env, ret);

}

/*
 * Class:     picamJNI_PICam
 * Method:    _getParameterRangeConstraint
 * Signature: (IILpicamJNI/PICamRangeConstraint;)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam__1getParameterRangeConstraint
  (JNIEnv *env, jobject obj, jint parameter, jint category, jobject constraintObj){
	PicamHandle camera = getModelHandle(env, obj);

	const PicamRangeConstraint *constraint = NULL;

	PicamError ret = Picam_GetParameterRangeConstraint(camera, (PicamParameter)parameter, (PicamConstraintCategory)category, &constraint);
	checkReturn(env, ret);

	if(constraint == NULL){
		printf("Picam_GetParameterRangeConstraint returned NULL constraint");
		return;
	}

	jclass constraintClass = env->GetObjectClass(constraintObj);
	jfieldID fieldID;

	fieldID = env->GetFieldID(constraintClass, "scope", "I");
	env->SetIntField(constraintObj, fieldID, constraint->scope);

	fieldID = env->GetFieldID(constraintClass, "severity", "I");
	env->SetIntField(constraintObj, fieldID, constraint->severity);

	fieldID = env->GetFieldID(constraintClass, "empty_set", "Z");
	env->SetBooleanField(constraintObj, fieldID, constraint->empty_set);

	fieldID = env->GetFieldID(constraintClass, "minimum", "D");
	env->SetDoubleField(constraintObj, fieldID, constraint->minimum);

	fieldID = env->GetFieldID(constraintClass, "maximum", "D");
	env->SetDoubleField(constraintObj, fieldID, constraint->maximum);

	fieldID = env->GetFieldID(constraintClass, "increment", "D");
	env->SetDoubleField(constraintObj, fieldID, constraint->increment);

	jdoubleArray valuesArr = env->NewDoubleArray(constraint->excluded_values_count);
	if (valuesArr != NULL){
		env->SetDoubleArrayRegion(valuesArr, 0, constraint->excluded_values_count, (double*)constraint->excluded_values_array);
	}
	fieldID = env->GetFieldID(constraintClass, "excluded_values", "[D");
	env->SetObjectField(constraintObj, fieldID, valuesArr);

	valuesArr = env->NewDoubleArray(constraint->outlying_values_count);
	if (valuesArr != NULL){
		env->SetDoubleArrayRegion(valuesArr, 0, constraint->outlying_values_count, (double*)constraint->outlying_values_array);
	}
	fieldID = env->GetFieldID(constraintClass, "outlying_values", "[D");
	env->SetObjectField(constraintObj, fieldID, valuesArr);

	Picam_DestroyRangeConstraints(constraint);
	

}




/*
 * Class:     picamJNI_PICam
 * Method:    _getParameterRoisValue
 * Signature: (I)[B
 */
JNIEXPORT jbyteArray JNICALL Java_picamJNI_PICam__1getParameterRoisValue
  (JNIEnv *env, jobject obj, jint parameter){

	const PicamRois *rois;
	PicamHandle camera = getModelHandle(env, obj);

	PicamError ret = Picam_GetParameterRoisValue(camera, (PicamParameter)parameter, &rois);
	checkReturn(env, ret);

	if(rois != NULL){

		int nBytes = rois->roi_count * sizeof(PicamRoi);
		jbyteArray retArr = env->NewByteArray(nBytes);
		if (retArr == NULL){
			fprintf(stderr, "PICam: getParameterRoisValue(): Couldn't allocate byte array in JNI land.");
			fflush(stderr);
			checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
			return NULL;
		}

		env->SetByteArrayRegion(retArr, 0, nBytes, (jbyte*)rois->roi_array);

		Picam_DestroyRois(rois);

		return retArr;
	}

	return NULL;
}

/*
 * Class:     picamJNI_PICam
 * Method:    _setParameterRoisValue
 * Signature: (I[B)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam__1setParameterRoisValue
  (JNIEnv *env, jobject obj, jint parameter, jbyteArray structData){

	PicamHandle camera = getModelHandle(env, obj);
	PicamRois rois;	

	jbyte *buffer = env->GetByteArrayElements(structData, 0);
	int nBytes = env->GetArrayLength(structData);

	rois.roi_array = (PicamRoi*)buffer;
	rois.roi_count = nBytes / sizeof(PicamRoi);

	PicamError ret = Picam_SetParameterRoisValue(camera, (PicamParameter)parameter, &rois);
	checkReturn(env, ret);

	env->ReleaseByteArrayElements(structData, buffer, 0);

}

//c-callback for Acqui updated
JavaVM *g_vm;
jobject ccbInterface;
jmethodID ccbMethod;
void *bufferPtr;
PicamAcquisitionBuffer ccbBufferInfo;


PicamError PIL_CALL ccbAcquisitionUpdated(
    PicamHandle device,
    const PicamAvailableData* available,
    const PicamAcquisitionStatus* status )
{
	JNIEnv *g_env;
	//printf("+ccbAcquisitionUpdated()\n"); fflush(stdout);
 
	if(ccbInterface == NULL){
        	fprintf(stderr, "ERROR: ccbAcquisitionUpdated() ccbInterface = null. Probably a late callback came after PicamAdvanced_UnregisterForAcquisitionUpdated\n");
		fflush(stderr);
		return PicamError_UnexpectedError;
	}
	//printf("ccbInterface check OK\n"); fflush(stdout);

	int getEnvStat = g_vm->GetEnv((void **)&g_env, JNI_VERSION_1_6);
	if (getEnvStat == JNI_EDETACHED) {
		if (g_vm->AttachCurrentThread((void **) &g_env, NULL) != 0) {
			fprintf(stderr, "GetEnv: Failed to attach\n"); fflush(stderr);
			return PicamError_UnexpectedError;
		}

	} else if (getEnvStat == JNI_OK) {
		//
	} else if (getEnvStat == JNI_EVERSION) {
		fprintf(stderr, "GetEnv: version not supported\n"); fflush(stderr);
		return PicamError_UnexpectedError;
	}

	//printf("VM Attach ok\n"); fflush(stdout);

	jint bufferPos, readout_count;
	if(available == NULL){
        	fprintf(stderr, "ERROR: ccbAcquisitionUpdated() available is null.\n");
		fflush(stderr);

		bufferPos = -1;
		readout_count = -1;
	}else{
		bufferPos = (ccbBufferInfo.memory == NULL) ? -1 : ((unsigned char*)available->initial_readout - (unsigned char*)ccbBufferInfo.memory);
		readout_count = available->readout_count;
	}
	

	jboolean running;
	jint errorsMask, readout_rate;
	if(status == NULL){
		fprintf(stderr, "ERROR: ccbAcquisitionUpdated() available is null.\n");
		fflush(stderr);
		
		running = false;
		errorsMask = -1;
		readout_rate = -1;
	}else{

		running = status->running;
		errorsMask = status->errors;
		readout_rate = status->readout_rate;
	}

	
        g_env->CallVoidMethod(ccbInterface, ccbMethod, bufferPos, readout_count, running, errorsMask, readout_rate);
	//printf("CallVoidMethod OK\n"); fflush(stdout);

	if(g_env->ExceptionOccurred()){
        	fprintf(stderr, "ERROR:  ccbAcquisitionUpdated() C-Callback caught a java exception in acquisitionUpdatedCallback()\n");
		fflush(stderr);
		g_env->ExceptionDescribe();
		return PicamError_UnexpectedError;
	}

	//printf("Error check OK\n"); fflush(stdout);

	g_vm->DetachCurrentThread();

	//printf("Detach OK\n"); fflush(stdout);

	return PicamError_None;
}

/*
 * Class:     picamJNI_PICam
 * Method:    registerForAcquisitionUpdated
 * Signature: (LpicamJNI/PICamAcquisitionUpdatedCallback;)V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_registerForAcquisitionUpdated
  (JNIEnv *env, jobject obj, jobject callbackObj){
	//printf("+Java_picamJNI_PICam_registerForAcquisitionUpdated()\n"); fflush(stdout);
 
	PicamHandle device = getDeviceHandle(env, obj);

	//printf("getDeviceHandle OK\n"); fflush(stdout);

	if(ccbInterface != NULL){
		fprintf(stderr, "PICam: registerForAcquisitionUpdated(): Callback already registered.\n");
		fflush(stderr);
		checkReturn(env, (PicamError)-73); //throw PICamSDKException("JNI alloction failed");
		return;
	}
	//printf("ccbInterface check OK\n"); fflush(stdout);

	env->GetJavaVM(&g_vm);
	ccbInterface = env->NewGlobalRef(callbackObj);
	jclass callbackClass = env->GetObjectClass(callbackObj);

	//printf("GetObjectClass OK, callbackClass=%p\n", callbackClass); fflush(stdout);

	static const char *sigStr = "(IIZII)V";
	ccbMethod = env->GetMethodID(callbackClass, "acquisitionUpdatedCallback", sigStr);
	//printf("GetMethodID OK, callbackClass=%p\n", callbackClass); fflush(stdout);

	ccbBufferInfo.memory = NULL;
	PicamAdvanced_GetAcquisitionBuffer(device, &ccbBufferInfo); //don't care if this fails

	//ccbEnv->CallVoidMethod(ccbInterface, ccbMethod, 0, 1, 2, 3);
	PicamError ret = PicamAdvanced_RegisterForAcquisitionUpdated(device, ccbAcquisitionUpdated); /* ASYNC */

	checkReturn(env, ret);
}


/*
 * Class:     picamJNI_PICam
 * Method:    unregisterForAcquisitionUpdated
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_picamJNI_PICam_unregisterForAcquisitionUpdated
  (JNIEnv *env, jobject obj){

	PicamHandle device = getDeviceHandle(env, obj);

	PicamError ret = PicamAdvanced_UnregisterForAcquisitionUpdated(device, ccbAcquisitionUpdated);
	ccbInterface = NULL;

	env->DeleteGlobalRef(ccbInterface);
	ccbInterface = NULL;

	bufferPtr = NULL;

	checkReturn(env, ret);
}
