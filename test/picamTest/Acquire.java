package picamTest;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import oneLiners.OneLiners;
import otherSupport.SettingsManager;
import picamJNI.PICam;
import picamJNI.PICamCollectionConstraint;
import picamJNI.PICamDefs;
import picamJNI.PICamParameters;
import picamJNI.PICamRangeConstraint;
import picamJNI.PICamSDKException;
import picamJNI.PicamCameraID;

/** Basic Acquisition Sample
/ The sample will open the first camera attached
/ and acquire 5 frames.  Part 2 of the sample will collect
/ 1 frame of data each time the function is called, looping
/ through 5 times.
 * 
 * 
 * 
 * 
*/
public class Acquire {
	public static final int NUM_FRAMES = 5;
	public static final int NO_TIMEOUT = -1;

	public static void printData(ByteBuffer buf, int numframes, int framelength )
	{
	    int midpt = 0;
	    int frameptr = 0;

	    for(int loop = 0; loop < numframes; loop++ ) {
	        frameptr = framelength * loop;
	        midpt = frameptr + framelength/2;
	        System.out.println(String.format("%5d,%5d,%5d\t%d\n", 
	        		buf.getShort(midpt-2),
	        		buf.getShort(midpt),  
	        		buf.getShort(midpt+2), 
	        		loop+1 ));
	    }
	}
	
	public static void main(String[] args) {
	
		new SettingsManager("minerva", true);
		
	    // - open the first camera if any or create a demo camera
	    PicamCameraID id;
	    
	    int readoutstride = 0;
	    
	    PICam camera;
	    try{
	    	//if(true)throw new PICamSDKException("Force demo");
	    	
	    	camera = PICam.OpenFirstCamera();
	    	id = camera.getCameraID();
	    	
	    }catch(PICamSDKException err){
	    	
	        id = PICam.ConnectDemoCamera(PicamCameraID.getModelID("Pixis100F"), "0008675309");
	        camera = PICam.OpenCamera(id);
	        System.out.println( "No Camera Detected, Creating Demo Camera\n" );
	    }
	    	    
	    System.out.print(id.getModelName());
	    System.out.println(" (SN:"+id.getSerialNumber()+") ["+id.getSensorName()+"])\n");
	    //Picam_DestroyString(str);

	    readoutstride = camera.getParameterIntegerValue(PICamParameters.ReadoutStride);
	    System.out.println( "Waiting for "+NUM_FRAMES+" frames to be collected.");
	    
	    int params[] = camera.getParameters();
	    for(int i=0; i < params.length; i++){
	    	
	    	System.out.print(String.format("%08x", params[i]) + ": " + PICamParameters.getName(params[i]) + " = ");
	    	if(PICamParameters.getType(params[i]) == PICamDefs.valueType_FloatingPoint){
	    		float val = camera.getParameterFloatingPointValue(params[i]);
	    		System.out.println(val + " (float)");
	    		
	    		int constraintType = PICamParameters.getConstraintType(params[i]);
	    		
	    		if(constraintType == PICamDefs.constraintType_Range){
	    			PICamRangeConstraint constraint = camera.getParameterRangeConstraint(params[i], PICamDefs.constraintCategory_Required);
	    			System.out.println(constraint);
	    			
	    		}else if(constraintType == PICamDefs.constraintType_Collection){
	    			PICamCollectionConstraint constraint = camera.getParameterCollectionConstraint(params[i], PICamDefs.constraintCategory_Required);
	    			System.out.println(constraint);
	    		}
	    		

	    	}else if(PICamParameters.getType(params[i]) == PICamDefs.valueType_Integer){
	    		int val = camera.getParameterIntegerValue(params[i]);
	    		System.out.println(val + " (int)");

	    	}else if(PICamParameters.getType(params[i]) == PICamDefs.valueType_LargeInteger){
	    		long val = camera.getParameterLargeIntegerValue(params[i]);
	    		System.out.println(val + " (long)");
	    		
	    	}else if(PICamParameters.getType(params[i]) == PICamDefs.valueType_Enumeration){
	    		int enumType = camera.GetParameterEnumeratedType(params[i]);
	    		int val = camera.getParameterIntegerValue(params[i]);
	    		String str = camera.GetEnumerationString(enumType, val);
	    		System.out.println(val + " (Enum, type="+enumType+", str=" + str + ")");
	    		
	    		for(int j=0; j < 100; j++){
	    			try{
	    				System.out.println("Enum "+j+"= " + camera.GetEnumerationString(enumType, j));
	    			}catch(PICamSDKException err){
	    				if(err.getErrorCode() != PICamSDKException.ENUM_NOT_DEFINED)
	    					throw err;
	    				if(j > 0)
	    					break;
	    			}
	    		}
	    		
	    	}else if(PICamParameters.getType(params[i]) == PICamDefs.valueType_Boolean){
	    		int val = camera.getParameterIntegerValue(params[i]);
	    		System.out.println((val == 0 ? "False" : "True") + " (bool)");
	    		
	    	}else{
	    		System.out.println("????");
	    	}
	    }
	    
	    int iRet[] = new int[2];
	    ByteBuffer buff = camera.acquire(NUM_FRAMES, NO_TIMEOUT, iRet);
	    
	    System.out.println( "Center Three Points:\tFrame # \n");
	    printData(buff, NUM_FRAMES, readoutstride );
	    
	    //collect NUM_FRAMES one at a time	    
	    System.out.println("Collecting 1 frame, looping "+NUM_FRAMES+" times." );
	    for(int i = 0; i < NUM_FRAMES; i++ )
	    {
	    	buff = camera.acquire(1, NO_TIMEOUT, iRet );
	        printData(buff, 1, readoutstride );
	        
	    }
	    
	    camera.close();	    
		
	}
}
